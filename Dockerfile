FROM python:3.7-slim

ENV PYTHONUNBUFFERED=1
WORKDIR /acme_inc

COPY requirements.txt /acme_inc/
RUN  pip install -r requirements.txt

copy . /acme_inc
EXPOSE 8000
