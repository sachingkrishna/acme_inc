from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.contrib.auth import authenticate
from django.http.response import JsonResponse
from django.http.response import HttpResponseForbidden



class LoginView(APIView):
    """This class authenticates a user against 
    the username and password passed in the request
    and returns token on successful authentication
    """
    premission_classes = [AllowAny]

    def post(self, request):
        user = authenticate(username=request.data['username'], password=request.data['password'])
        if user:
            token, _ = Token.objects.get_or_create(user=user)
            print(token.__dict__)
            return JsonResponse({'token': token.key}, status=200)
        else:
            return HttpResponseForbidden('Invalid credentials')
