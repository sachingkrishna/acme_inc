from django.apps import AppConfig


class AcmeAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acme_auth'
