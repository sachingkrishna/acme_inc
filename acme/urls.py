from django.urls import path, include
from rest_framework import routers
from rest_framework.routers import DefaultRouter
from product import views as product_views
from acme_auth import views as auth_views


router = DefaultRouter()
router.register('products', product_views.ProductViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('login/', auth_views.LoginView.as_view())
]
