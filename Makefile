build:
	docker build --force-rm $(options) -t acme:latest .

acme-start:
	docker-compose up --remove-orphans $(options)

acme-stop:
	docker-compose down --remove-orphans $(options)

acme-manage:
	docker-compose run --rm $(options) web python manage.py $(cmd)